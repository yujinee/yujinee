---
layout: post
title:  "버튼 태그(button) 구분"
date:   2016-03-24 15:32:14 -0300
categories: jekyll update
---

1. 페이지 이동
{% highlight ruby %}
    <a href=" ">
{% endhighlight %}
페이지 이동,페이지 링크를 걸때 주로 사용(이동하지않을땐 사용하지않는 것이 좋다)
서브로그인을 비롯하여 여러가지 기능을 넣을때 사용


Check out the [Jekyll docs][jekyll-docs] for more info on how to get the most out of Jekyll. File all bugs/feature requests at [Jekyll’s GitHub repo][jekyll-gh]. If you have questions, you can ask them on [Jekyll Talk][jekyll-talk].

[jekyll-docs]: http://jekyllrb.com/docs/home
[jekyll-gh]:   https://github.com/jekyll/jekyll
[jekyll-talk]: https://talk.jekyllrb.com/
